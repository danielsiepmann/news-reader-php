<?php

namespace App\Tests\Service;

/*
 * Copyright (C) 2020 Daniel Siepmann <coding@daniel-siepmann.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

use App\Service\OpmlImporter;
use PHPUnit\Framework\TestCase;
use SimpleXMLElement;

class OpmlImporterTest extends TestCase
{
    /**
     * @test
     */
    public function detectsBucket()
    {
        $subject = new OpmlImporter();
        $simpleXmlElement = new SimpleXmlElement('<outline title="Some title"></outline>');

        static::assertTrue($subject->isBucket($simpleXmlElement));
    }

    /**
     * @test
     */
    public function detectsFeedIsNoBucket()
    {
        $subject = new OpmlImporter();
        $simpleXmlElement = new SimpleXmlElement('<outline type="rss" xmlUrl="http://typo3.org/xml-feeds/rss.xml"/>');

        static::assertFalse($subject->isBucket($simpleXmlElement));
    }

    /**
     * @test
     */
    public function detectsFeed()
    {
        $subject = new OpmlImporter();
        $simpleXmlElement = new SimpleXmlElement('<outline type="rss" xmlUrl="http://typo3.org/xml-feeds/rss.xml"/>');

        static::assertTrue($subject->isFeed($simpleXmlElement));
    }

    /**
     * @test
     */
    public function detectsBucketIsNoFeed()
    {
        $subject = new OpmlImporter();
        $simpleXmlElement = new SimpleXmlElement('<outline title="Some title"></outline>');

        static::assertFalse($subject->isFeed($simpleXmlElement));
    }
}
