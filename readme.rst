News Reader in PHP
==================

A small private project to play around with Frontend (HTML + CSS),
as well with Symfony.

The goal is to provide a small self hosted web application to read news from RSS Feeds.

Feeds can be grouped into buckets.

Feeds should be added via web ui, as well imported via OPML from command line.

Let's see where it goes.

List of ideas
-------------

Those ideas might be implemented sooner or later, or never.

* Don't import entries more then once.

* Aggregate entries from feeds in "bucket" view.

* Allow entries to be marked as read.

* Allow to hide read entries.

* Add form to add new feeds or import file via web ui.

* Allow to delete and rename feeds.

* Think about font sizes:
  https://www.paritybit.ca/blog/a-quick-rant-about-web-font-sizes

* Think about font types:
  https://jlelse.blog/dev/sans-serif-only/

* Add links to "up", "next", etc. where appropriate.
  https://developer.mozilla.org/en-US/docs/Web/HTML/Link_types

* Add output for commands, e.g. progress bar.

* Add logging, e.g. which entry was imported.

* Make use able on mobile devices. Maybe move content in sidebar to start page, it is
  basically following the existing navigation then.
  Use sidebar for related things, e.g. interacting with content.
  Maybe move sidebar to the left then, and to the bottom on small view ports?

* Add more tests

