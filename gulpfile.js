const autoprefixer = require('autoprefixer');
const concat = require('gulp-concat');
const del = require('delete');
const path = require('path');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const {src, dest, series, watch} = require('gulp');

const destDirs = {
    css: path.join('public', 'css'),
};
const srcDir = path.join('assets');
const srcDirs = {
    sass: path.join(srcDir, 'sass'),
};

function cssTask() {
    const sources = [
        path.join('node_modules',),
        path.join(srcDirs.sass, 'index.scss'),
    ];
    const postCssPlugins = [
        autoprefixer(),
    ];

    return src(sources)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(postCssPlugins))
        .pipe(concat('style.css'))
        .pipe(dest(destDirs.css));
}

function cleanTask(cb) {
    del([
        destDirs.css,
    ], cb);
}

function watchTask() {
    watch([srcDirs.sass], {ignoreInitial: false}, cssTask);
}

exports.css = cssTask;
exports.default = series(cleanTask, cssTask);
exports.watch = watchTask;
