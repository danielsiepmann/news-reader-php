<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200528222741 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE entry (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, feed_id INTEGER DEFAULT NULL, public_id VARCHAR(255) NOT NULL, published DATETIME NOT NULL --(DC2Type:datetime_immutable)
        , name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, category VARCHAR(255) NOT NULL, content CLOB NOT NULL, read BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_2B219D7051A5BC03 ON entry (feed_id)');
        $this->addSql('CREATE UNIQUE INDEX entry_routing ON entry (slug)');
        $this->addSql('DROP INDEX feed_routing');
        $this->addSql('DROP INDEX IDX_234044AB84CE584D');
        $this->addSql('CREATE TEMPORARY TABLE __temp__feed AS SELECT id, bucket_id, name, slug, url, html_url FROM feed');
        $this->addSql('DROP TABLE feed');
        $this->addSql('CREATE TABLE feed (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bucket_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, slug VARCHAR(255) NOT NULL COLLATE BINARY, url VARCHAR(255) NOT NULL COLLATE BINARY, html_url VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_234044AB84CE584D FOREIGN KEY (bucket_id) REFERENCES bucket (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO feed (id, bucket_id, name, slug, url, html_url) SELECT id, bucket_id, name, slug, url, html_url FROM __temp__feed');
        $this->addSql('DROP TABLE __temp__feed');
        $this->addSql('CREATE UNIQUE INDEX feed_routing ON feed (slug)');
        $this->addSql('CREATE INDEX IDX_234044AB84CE584D ON feed (bucket_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE entry');
        $this->addSql('DROP INDEX IDX_234044AB84CE584D');
        $this->addSql('DROP INDEX feed_routing');
        $this->addSql('CREATE TEMPORARY TABLE __temp__feed AS SELECT id, bucket_id, name, slug, url, html_url FROM feed');
        $this->addSql('DROP TABLE feed');
        $this->addSql('CREATE TABLE feed (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bucket_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, html_url VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO feed (id, bucket_id, name, slug, url, html_url) SELECT id, bucket_id, name, slug, url, html_url FROM __temp__feed');
        $this->addSql('DROP TABLE __temp__feed');
        $this->addSql('CREATE INDEX IDX_234044AB84CE584D ON feed (bucket_id)');
        $this->addSql('CREATE UNIQUE INDEX feed_routing ON feed (slug)');
    }
}
