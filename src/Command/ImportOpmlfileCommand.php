<?php

namespace App\Command;

use App\Service\OpmlImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportOpmlfileCommand extends Command
{
    /**
     * @var OpmlImporter
     */
    private $importer;

    public function __construct(
        OpmlImporter $importer
    ) {
        parent::__construct();
        $this->importer = $importer;
    }

    protected function configure()
    {
        $this->setDescription('Imports an OPML file.');
        $this->addArgument('file', InputArgument::REQUIRED, 'The OPML file to import.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->importer->importFromFile(
            $input->getArgument('file')
        );

        return 0;
    }
}
