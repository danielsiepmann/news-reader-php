<?php

namespace App\Command;

use App\Repository\FeedRepository;
use App\Service\FeedEntriesImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportFeedEntriesCommand extends Command
{
    /**
     * @var FeedEntriesImporter
     */
    private $importer;

    /**
     * @var FeedRepository
     */
    private $repository;

    public function __construct(
        FeedEntriesImporter $importer,
        FeedRepository $repository
    ) {
        parent::__construct();
        $this->importer = $importer;
        $this->repository = $repository;
    }

    protected function configure()
    {
        $this->setDescription('Imports new entries from all known feeds.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->repository->findAll() as $feed) {
            $this->importer->importEntries($feed);
        }

        return 0;
    }
}
