<?php

namespace App\Entity;

use App\Repository\EntryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EntryRepository::class)
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="entry_routing", columns={"slug"})})
 */
class Entry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Feed::class, inversedBy="entries")
     */
    private $feed;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $publicId;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private $read = false;

    public function __construct(
        Feed $feed,
        string $publicId,
        \DateTimeImmutable $published,
        string $name,
        string $slug,
        string $url,
        string $category,
        string $content
    ) {
        $this->feed = $feed;
        $this->publicId = $publicId;
        $this->published = $published;
        $this->name = $name;
        $this->slug = $slug;
        $this->url = $url;
        $this->category = $category;
        $this->content = $content;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFeed(): Feed
    {
        return $this->feed;
    }

    public function getPublicId(): string
    {
        return $this->publicId;
    }

    public function getPublished(): \DateTimeImmutable
    {
        return $this->published;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    // TODO: Rename into is / was ?
    public function getRead(): bool
    {
        return $this->read;
    }
}
