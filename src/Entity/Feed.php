<?php

namespace App\Entity;

use App\Repository\FeedRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FeedRepository::class)
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="feed_routing", columns={"slug"})})
 */
class Feed
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $htmlUrl;

    /**
     * @ORM\ManyToOne(targetEntity=Bucket::class, inversedBy="feeds")
     */
    private $bucket;

    /**
     * @ORM\OneToMany(targetEntity=Entry::class, mappedBy="feed")
     * @ORM\OrderBy({"published" = "DESC"})
     */
    private $entries;

    public function __construct(
        string $name,
        string $slug,
        string $url,
        Bucket $bucket = null,
        string $htmlUrl = ''
    ) {
        $this->name = $name;
        $this->slug = $slug;
        $this->url = $url;
        $this->bucket = $bucket;
        $this->htmlUrl = $htmlUrl;
        $this->entries = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getBucket(): Bucket
    {
        return $this->bucket;
    }

    public function getHtmlUrl(): string
    {
        return $this->htmlUrl;
    }

    public function getEntries(): Collection
    {
        return $this->entries;
    }
}
