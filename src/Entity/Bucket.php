<?php

namespace App\Entity;

use App\Repository\BucketRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BucketRepository::class)
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="bucket_routing", columns={"slug"})})
 */
class Bucket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=Feed::class, mappedBy="bucket")
     */
    private $feeds;

    public function __construct(
        string $name,
        string $slug
    ) {
        $this->name = $name;
        $this->slug = $slug;
        $this->feeds = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return Collection|Feed[]
     */
    public function getFeeds(): Collection
    {
        return $this->feeds;
    }
}
