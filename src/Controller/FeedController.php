<?php

namespace App\Controller;

use App\Entity\Feed;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FeedController extends AbstractController
{
    public function show(Feed $feed)
    {
        return $this->render('feed/show.html.twig', [
            'feed' => $feed,
        ]);
    }
}
