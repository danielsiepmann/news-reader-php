<?php

namespace App\Controller;

use App\Entity\Bucket;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BucketController extends AbstractController
{
    public function show(Bucket $bucket)
    {
        return $this->render('bucket/index.html.twig', [
            'bucket' => $bucket,
        ]);
    }
}
