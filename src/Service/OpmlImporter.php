<?php

namespace App\Service;

/*
 * Copyright (C) 2020 Daniel Siepmann <coding@daniel-siepmann.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

use App\Entity\Bucket;
use App\Entity\Feed;
use Doctrine\ORM\EntityManagerInterface;
use SimpleXMLElement;
use Symfony\Component\String\Slugger\SluggerInterface;

class OpmlImporter
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager
    ) {
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
    }

    public function importFromFile(string $filePath): void
    {
        $opml = new SimpleXMLElement(file_get_contents($filePath));

        // TODO: Trigger event

        foreach ($opml->body->outline as $outline) {
            $this->importOutline($outline);
        }

        $this->entityManager->flush();
    }

    public function isBucket(SimpleXMLElement $outline): bool
    {
        return is_null($outline['type'])
            && (
                isset($outline['text'])
                || isset($outline['title'])
            )
            ;
    }

    public function isFeed(SimpleXMLElement $outline): bool
    {
        return isset($outline['type'])
            && $outline['type']->__toString() === 'rss'
            && isset($outline['xmlUrl'])
            ;
    }

    private function importOutline(SimpleXMLElement $outline, Bucket $bucket = null): void
    {
        if ($this->isBucket($outline)) {
            $this->importBucket($outline);
            return;
        }

        if ($this->isFeed($outline)) {
            $this->importFeed($outline, $bucket);
            return;
        }
    }

    private function importBucket(SimpleXMLElement $outline): void
    {
        $title = $outline['title'] ?? $outline['text'];

        // Check whether bucket already exists.

        $bucket = new Bucket(
            $title,
            strtolower($this->slugger->slug($title))
        );

        // TODO: Trigger event

        $this->entityManager->persist($bucket);

        foreach ($outline->outline as $subOutline) {
            $this->importOutline($subOutline, $bucket);
        }
    }

    private function importFeed(SimpleXMLElement $outline, Bucket $bucket = null): void
    {
        $title = $outline['title'] ?? $outline['text'];

        $feed = new Feed(
            $title,
            strtolower($this->slugger->slug($title)),
            $outline['xmlUrl'],
            $bucket,
            $outline['htmlUrl'] ?? ''
        );

        // TODO: Trigger event

        $this->entityManager->persist($feed);
    }
}
