<?php

namespace App\Service;

/*
 * Copyright (C) 2020 Daniel Siepmann <coding@daniel-siepmann.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

use App\Entity\Entry;
use App\Entity\Feed;
use Doctrine\ORM\EntityManagerInterface;
use SimplePie;
use SimplePie_Category;
use SimplePie_Item;
use Symfony\Component\String\Slugger\SluggerInterface;

class FeedEntriesImporter
{
    /**
     * @var FeedParserFactory
     */
    private $parserFactory;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(
        FeedParserFactory $parserFactory,
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger
    ) {
        $this->parserFactory = $parserFactory;
        $this->entityManager = $entityManager;
        $this->slugger = $slugger;
    }

    public function importEntries(Feed $feed): void
    {
        $parser = $this->parserFactory->getParser();
        $parser->set_feed_url($feed->getUrl());
        $parser->init();

        // TODO: Send event
        // dump($feed->getUrl());

        foreach ($parser->get_items() as $item) {
            // TODO: Send event
            // TODO: Check publishing date and skip if to old
            $this->importEntry($item, $feed);
        }

        $this->entityManager->flush();
    }

    private function importEntry(SimplePie_Item $item, Feed $feed): void
    {
        $entry = new Entry(
            $feed,
            $item->get_id(),
            $this->getDateTime($item->get_gmdate()),
            $item->get_title(),
            $this->slugger->slug($item->get_title()),
            $item->get_link() ?? '',
            $this->implodeCategories($item->get_categories()),
            $item->get_content(),
        );
        $this->entityManager->persist($entry);
    }

    private function implodeCategories(?array $categories = null): string
    {
        if ($categories === null) {
            return '';
        }

        return implode(',', array_map(function (SimplePie_Category $category) {
            return $category->get_term();
        }, $categories));
    }

    private function getDateTime($dateTime): \DateTimeImmutable
    {
        $dateTime = \DateTimeImmutable::createFromFormat(
            'd M Y, h:i a',
            $dateTime,
            new \DateTimeZone('GMT')
        );

        return $dateTime->setTimezone(new \DateTimeZone(date_default_timezone_get()));
    }
}
