<?php

namespace App\DataFixtures;

use App\Entity\Bucket;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        $buckets = [
            'TYPO3 (official)',
            'TYPO3 (related)',
            'Updates',
            'Linux',
            'Work',
            'New',
            'tech',
            'Random',
            'Music',
            'Sport',
            'Games',
            'Coding',
            'Vim',
            'Magazine',
            'Podcasts',
        ];

        foreach ($buckets as $bucketName) {
            $bucket = new Bucket(
                $bucketName,
                strtolower($this->slugger->slug($bucketName))
            );
            $manager->persist($bucket);
        }

        $manager->flush();
    }
}
